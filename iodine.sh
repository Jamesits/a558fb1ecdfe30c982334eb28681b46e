#!/bin/bash
sudo apt install zlib1g zlib1g-dev
cd /usr/local/src
sudo git clone https://github.com/yarrick/iodine.git
cd iodine
sudo make
sudo make install